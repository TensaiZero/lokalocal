import 'package:cloud_firestore/cloud_firestore.dart';

class CustomerModel {
  String docID;
  int customerID = 0;
  String name = '';
  double balance = 0;
  double grams = 0;
  String dateActive = '';
  bool update= false;

  void addCustomer() {
    Firestore.instance.collection('customers').document().setData({
      'active': true,
      'customerID': 123,
      'date_active': '11/15/2018',
      'grams_consumed': '0',
      'loft_balance': '1000',
      'name': 'Paul Laiz',
      'passkey': ''
    });
  }

  void parseData(dynamic o, Function(CustomerModel) s) {
    this.docID = o.documentID;
    this.name = o["name"];
    this.customerID = o['customerID'];
    this.balance = double.tryParse(o['loft_balance']);
    this.grams = double.tryParse(o['grams_consumed']);
    this.dateActive = o['date_active'];
    if (update){
      s(this);
    }
  }

  void getCustomerData(Function(CustomerModel) done) {
    update= true;
    print('get customer data');
    Firestore.instance
        .collection('customers')
        .where('customerID', isEqualTo: customerID)
        .snapshots()
        .listen((data) => parseData(data.documents[0], done));
  }

  void updateBalance(double d,double g) {
    update = false;
    Firestore.instance.collection('customers').document(docID).setData({
      'active': true,
      'customerID': customerID,
      'date_active': dateActive,
      'grams_consumed': (grams + g).toString(),
      'name': name,
      'passkey': '',
      'loft_balance': (balance - d).toString()
    });
    balance -= d;
  }
}
