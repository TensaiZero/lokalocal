import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:loftpartnerapp/loftportalresources.dart';

class BrewItemModel {
  String name = "";
  String coffeeBean = "";
  String unit = "";
  double quantity = 0;
  double price = 0;
  String size = "";

  //Get BrewItemList
  static void getItems(Function(List<BrewItemModel>) r) {
    Firestore.instance
        .collection('brews')
        //TODO Add filter
        //.where('partnerID', isEqualTo: IoC().appVM.currentUser.id )
        .snapshots()
        .listen((data) => parseItems(data.documents, r));
  }

  static void parseItems(
      List<DocumentSnapshot> d, Function(List<BrewItemModel>) r) {
    var result = List<BrewItemModel>();
    d.forEach((f) => result.add(BrewItemModel()..parseData(f.data)));
    r(result);
  }

  void addNew() {
    Firestore.instance.collection('brews').document().setData({
      'bean': coffeeBean,
      'brewID': name,
      'cost': price.toString(),
      'partnerID': IoC().appVM.currentUser.id,
      'quantity': quantity.toString(),
      'size': size,
      'unit': unit
    });
  }

  void parseData(dynamic o) {
    name = o['brewID'];
    coffeeBean = o['bean'];
    unit = o['unit'];
    quantity = double.tryParse(o['quantity']);
    price = double.tryParse(o['cost']);
    size = o['size'];
  }
}
