import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:loftpartnerapp/loftportalresources.dart';

class TransactionModel {
  int customerID;
  String custName;
  String partnerName;
  int partnerID = IoC().appVM.currentUser.id;
  String branch = IoC().appVM.currentUser.branch;
  String date;
  String time;
  String brewID;
  double quantity = 0;
  double price;
  double total;

  BrewItemModel item;

//Get TransactionList
  static void getItems(Function(List<TransactionModel>) r) {
    Firestore.instance
        .collection('transactions')
        //TODO Add filter
        //.where('partnerID', isEqualTo: IoC().appVM.currentUser.id )
        .snapshots()
        .listen((data) => parseItems(data.documents, r));
  }

  static void parseItems(
      List<DocumentSnapshot> d, Function(List<TransactionModel>) r) {
    var result = List<TransactionModel>();
    d.forEach((f) => result.insert(0, TransactionModel()..parseData(f.data)));
    r(result);
  }

  void parseData(dynamic o){
    customerID = o['customerID'];
    custName = o['customerName'];
    partnerName = o['partnerName'];
    date = o['date'];
    time = o['time'];
    branch = o['branch'];
    item = BrewItemModel();
    item.parseData(o);
    quantity = double.tryParse(o['order_qty']);
    price = double.tryParse(o['cost']);
    total = quantity * price;
  }

  void saveTransaction() {
    print('saving data');
    Firestore.instance.collection('transactions').document().setData({
      'customerID': customerID,
      'customerName' : custName,
      'date': DateTime.now().month.toString() +
          '/' +
          DateTime.now().day.toString() +
          '/' +
          DateTime.now().year.toString(),
      'time': DateTime.now().hour.toString() +
          ':' +
          DateTime.now().minute.toString().padLeft(2, '0'),
      'branch': IoC().appVM.currentUser.branch,
      'partnerID': IoC().appVM.currentUser.id,
      'partnerName': IoC().appVM.currentUser.name,
      'brewID': item.name,
      'bean': item.coffeeBean,
      'quantity': item.quantity.toString(),
      'unit': item.unit,
      'size': item.size,
      'cost': price.toString(),
      'order_qty': quantity.toString(),
    });
  }

  void setItem(BrewItemModel i) {
    item = i;
    brewID = item.name;
    price = item.price;
    addQuantity();
  }

  void addQuantity() {
    quantity++;
    total = price * quantity;
  }
}
