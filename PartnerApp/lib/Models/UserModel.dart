import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  int id = 100045;
  String name = 'Ellipsis';
  String passkey;
  String branch = 'ORTIGAS';

  void getUserData(Function(UserModel) done) {
    Firestore.instance
        .collection('partners')
        .where('partnerID', isEqualTo: id)
        .snapshots()
        .listen((data) { 
          print(id);
          print(data.documents.length);
          if(data.documents.length > 0){
            parseData(data.documents[0], done);
          }else{
            print('error');
            done(null);
          }
          });
  }

  void parseData(dynamic o, Function(UserModel) s) {
    this.name = o["name"];
    this.branch = o['branch'];
    s(this);
  }
}
