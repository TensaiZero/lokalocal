import 'package:flutter/material.dart';
//IoC
export 'IoC/IoC.dart';
//Animation
export 'Animation/scalerouteAnim.dart';
//ViewModels
export 'ViewModels/applicationVM.dart';
//Screens
export 'Screens/cashierScreen.dart';
export 'Screens/dashboardScreen.dart';
export 'Screens/brewEditorScreen.dart';
export 'Screens/SalesScreen.dart';
export 'Screens/loginScreen.dart';
//Widgets
export 'Widgets/buttons.dart';
export 'Widgets/loading.dart';
export 'Widgets/inputs.dart';
//Models
export 'Models/CustomerModel.dart';
export 'Models/UserModel.dart';
export 'Models/BrewItemModel.dart';
export 'Models/TransactionModel.dart';

//Colors
final mainColor = Color(0xFF2DA1DB);
final colorGreen = Color(0xFF68C03A);
