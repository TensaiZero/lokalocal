import 'package:flutter/material.dart';
import 'package:loftpartnerapp/loftportalresources.dart';

class AppVM {
  static final AppVM _singleton = new AppVM._internal();

  StatefulWidget currentPage = LoginScreen(); //Current Page
  //UserModel currentUser = UserModel(); //Current User
  //MainPage mainPage = MainPage();
  //SettingsModel currentSettings = SettingsModel();

  factory AppVM(){
    return _singleton;
  }

  AppVM._internal();

  StatefulWidget getCurrentPage() => currentPage;
  UserModel currentUser = UserModel(); //Current User
  //MainPage getMainPage() => mainPage;

  void showSnackBar(String message, Color color){
  }
}