import 'package:flutter/material.dart';
import 'package:loftpartnerapp/loftportalresources.dart';

class SalesScreen extends StatefulWidget {
  @override
  _SalesScreenState createState() => _SalesScreenState();
}

class _SalesScreenState extends State<SalesScreen> {
  List<TransactionModel> transactions = List<TransactionModel>();

  _SalesScreenState() {
    TransactionModel.getItems(setItems);
  }

  double totalSales() {
    double t = 0;
    transactions.forEach((f) {
      t += f.total;
    });
    return t;
  }

  void setItems(List<TransactionModel> t) {
    if (mounted) {
      setState(() {
        transactions = t;
      });
    }
  }

  Widget headerText(String s, double w) {
    return Container(
      width: w,
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Center(
          child: Text(
        s,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      )),
    );
  }

  Widget historyItems(TransactionModel t) {
    return Card(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        child: Row(children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                t.custName,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Text(t.partnerName + ' - ' + t.branch),
              Text(t.date + ' ' + t.time)
            ],
          ),
          Expanded(
            child: Container(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                t.total.toString(),
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(t.item.coffeeBean +
                  ' (' +
                  (t.quantity * t.item.quantity).toString() +
                  t.item.unit +
                  ')')
            ],
          )
        ]),
        height: 60.0,
      ),
    );
  }

  Widget salesHistory() {
    return Card(
      child: Container(
          width: 700.0,
          child: Column(
            children: <Widget>[
              //Header
              Card(
                  child: Container(
                height: 40,
                color: mainColor,
                width: double.infinity,
                child: headerText('Recent Transactions', double.infinity),
              )),
              Expanded(
                  child: ListView(
                children: <Widget>[]..addAll(transactions.map((f) {
                    return historyItems(f);
                  }).toList()),
              )),
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        title: Text('Sales Report'),
      ),
      body: Row(children: <Widget>[
        salesHistory(),
        //Summary
        Column(children: <Widget>[
          Card(
            child: Container(
              width: 560,
              height: 365,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'TOTAL SALES',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 25.0),
                      ),
                    ),
                    Center(
                        child: Text(
                      totalSales().toString(),
                      style:
                          TextStyle(fontSize: 120, fontWeight: FontWeight.bold),
                    ))
                  ]),
            ),
          ),
          Card(
            child: Container(
              width: 560,
              height: 362,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'TOTAL DONATIONS (5%)',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 25.0),
                      ),
                    ),
                    Center(
                        child: Text(
                      (totalSales() * 0.05).toString(),
                      style:
                          TextStyle(fontSize: 120, fontWeight: FontWeight.bold),
                    ))
                  ]),
            ),
          ),
        ])
      ]),
    );
  }
}
