import 'package:flutter/material.dart';
import 'package:loftpartnerapp/loftportalresources.dart';

class BrewEditorScreen extends StatefulWidget {
  @override
  _BrewEditorScreenState createState() => _BrewEditorScreenState();
}

class _BrewEditorScreenState extends State<BrewEditorScreen> {
  List<BrewItemModel> _items = List<BrewItemModel>();
  bool _new;
  double sideBarWidth = 0;
  bool _isLoading = true;
  BrewItemModel selectedItem = BrewItemModel();

  //Constuctor
  _BrewEditorScreenState() {
    //Initialize Brew List
    BrewItemModel.getItems(setItems);
  }

  void setItems(List<BrewItemModel> b) {
    setState(() {
      //Set Items
      _items = b;
      _isLoading = false;
    });
  }

  void addItem() {
    setState(() {
      if (_new) {
        selectedItem.addNew();
        _items.add(selectedItem);
      } else {}
      hideSideBar();
    });
  }

  //ShowSideBar
  void showSideBar() {
    setState(() {
      sideBarWidth = 500;
    });
  }

  void hideSideBar() {
    setState(() {
      print('Hide Sidebar');
      sideBarWidth = 0;
    });
  }

  //New Brew Item
  void newItem() {
    _new = true;
    selectedItem = BrewItemModel();
    showSideBar();
  }

  void editItem(BrewItemModel b) {
    _new = false;
    selectedItem = b;
    showSideBar();
  }

  //Brew Editor Body
  Widget brewMenuBody() {
    return Stack(children: <Widget>[
      ListView(children: <Widget>[
        Wrap(
            children: <Widget>[]..addAll(_items.map((f) {
                return BrewItemButton(
                  model: f,
                  icon: Icons.access_alarms,
                  text: f.name,
                  onPressed: (b) => editItem(b),
                );
              }).toList()))
      ]),
      //Add Item Action Button
      Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: const EdgeInsets.all(30.0),
            child: FloatingActionButton(
              onPressed: () => newItem(),
              child: Icon(Icons.add),
            ),
          ))
    ]);
  }

  //SideBar Body
  Widget sidebarBody() {
    return Column(children: <Widget>[
      //Header
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Brew Details',
            style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.bold)),
      ),
      //Inputs
      ZTextInput(
        placeHolder: 'Brew ID',
        value: selectedItem.name,
        onChange: (s) => selectedItem.name = s,
      ),
      ZTextInput(
        placeHolder: 'Coffee bean',
        value: selectedItem.coffeeBean,
        onChange: (s) => selectedItem.coffeeBean = s,
      ),
      ZTextInput(
        placeHolder: 'Size',
        value: selectedItem.size,
        onChange: (s) => selectedItem.size = s,
      ),
      ZTextInput(
        placeHolder: 'Unit',
        value: selectedItem.unit,
        onChange: (s) => selectedItem.unit = s,
      ),
      ZTextInput(
        placeHolder: 'Quantity',
        numberOnly: true,
        value: selectedItem.quantity.toString(),
        onChange: (s) => selectedItem.quantity = double.tryParse(s),
      ),
      ZTextInput(
        placeHolder: 'Price',
        numberOnly: true,
        value: selectedItem.price.toString(),
        onChange: (s) => selectedItem.price = double.tryParse(s),
      ),
      Expanded(child: Container()),
      //Save and Cancel Button
      Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          BasicButton(
            color: colorGreen,
            text: 'SAVE',
            onPressed: addItem,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: BasicButton(
              color: Colors.redAccent,
              text: 'CANCEL',
              onPressed: hideSideBar,
            ),
          ),
        ]),
      )
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          title: Text('Brew Editor'),
          backgroundColor: mainColor,
        ),
        body: Stack(children: <Widget>[
          Opacity(
            opacity: 0.3,
            child: Container(
                width: double.infinity,
                child: Image(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/3.jpg'),
                )),
          ),
          brewMenuBody(),
          Align(
              alignment: Alignment.centerRight,
              child: AnimatedContainer(
                color: mainColor,
                width: sideBarWidth,
                duration: Duration(milliseconds: 100),
                child: sidebarBody(),
              )),
          LoadingOverlay(
            isLoading: _isLoading,
          )
        ]));
  }
}
