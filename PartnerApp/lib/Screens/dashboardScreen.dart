import 'package:flutter/material.dart';
import 'package:loftpartnerapp/loftportalresources.dart';
import 'package:barcode_scan/barcode_scan.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  String barcode;
  bool isLoading = false;
  //Goto Brew Editor
  void gotoBrewEditor() {
    Navigator.push(context, ScaleRoute(widget: BrewEditorScreen()));
  }

  //Goto Cashier Screen
  void gotoCashier(CustomerModel c) {
    Navigator.push(context, ScaleRoute(widget: CashierScreen(customer: c)));
  }

  //Goto Sales Page
  void gotoSalesPage() {
    Navigator.push(context, ScaleRoute(widget: SalesScreen()));
  }

  //Dashboard Body Section
  Widget dashboardBody() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  DashboardButton(
                      onPressed: () => scan(),
                      text: 'New Transaction',
                      icon: Icons.add),
                  DashboardButton(
                      onPressed: () => gotoBrewEditor(),
                      text: 'Brew Editor',
                      icon: Icons.edit),
                  DashboardButton(
                      onPressed: () => gotoSalesPage(),
                      text: 'Sales',
                      icon: Icons.monetization_on),
                ]),
          )
        ],
      ),
    );
  }

  //QR Code Scan Result
  void scanResult(CustomerModel c) {
    setState(() {
      isLoading = false;
    });
    if (c == null) {
      print('Customer not found');
    } else {
      gotoCashier(c);
      //Goto Next Page
      print('Goto Next Page');
    }
  }

  //No QR Code Scane
  void noBarcodeScanned() {
    setState(() {
      isLoading = false;
    });
  }

  //QR Code Scan Method
  Future scan() async {
    setState(() {
      isLoading = true;
    });
    try {
      barcode = await BarcodeScanner.scan();
    } catch (e) {
      noBarcodeScanned();
      return;
    }
    CustomerModel customer = CustomerModel();
    customer.customerID = int.tryParse(barcode);
    customer.getCustomerData(scanResult);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Scaffold(
            appBar: AppBar(
              title: Text('Dashboard'),
              backgroundColor: Color(0xFF2DA1DB),
            ),
            body: Stack(children: <Widget>[
              Opacity(
                opacity: 0.3,
                child: Container(
                  width: double.infinity,
                  color: Colors.black,
                  child: Image(
                    image: AssetImage('assets/1.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              dashboardBody(),
              LoadingOverlay(isLoading: isLoading)
            ])),
        Align(
          alignment: Alignment.topCenter,
          child: Container(
            margin: EdgeInsets.only(top: 5),
            height: 40,
            child: Image(
              image: AssetImage('assets/logo.png'),
            ),
          ),
        )
      ],
    );
  }
}
