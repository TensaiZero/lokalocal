import 'package:flutter/material.dart';
import 'package:loftpartnerapp/loftportalresources.dart';

class CashierScreen extends StatefulWidget {
  final CustomerModel customer;

  CashierScreen({this.customer});

  @override
  _CashierScreenState createState() => _CashierScreenState(customer: customer);
}

class _CashierScreenState extends State<CashierScreen> {
  final CustomerModel customer;
  List<BrewItemModel> _items = List<BrewItemModel>();
  bool _isLoading = true;
  double tot;
  double grams;

  List<TransactionModel> transItems = List<TransactionModel>();

  _CashierScreenState({this.customer}) {
    BrewItemModel.getItems(setItems);
  }

  void setItems(List<BrewItemModel> b) {
    if (mounted) {
      setState(() {
        //Set Items
        _items = b;
        _isLoading = false;
      });
    }
  }

  double totalAmount() {
    tot = 0;
    grams = 0;
    transItems.forEach((f) {
      tot += f.total;
      grams += f.item.quantity * f.quantity;
    });
    return tot;
  }

  Widget header() {
    //customer.addCustomer();
    return Container(
        padding: EdgeInsets.all(20.0),
        color: mainColor,
        height: 120,
        width: double.infinity,
        child: Stack(
            fit: StackFit.expand,
            alignment: Alignment.center,
            children: <Widget>[
              //Customer Details
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'CUSTOMER',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    Text(customer.name,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 40,
                            fontWeight: FontWeight.bold))
                  ]),
              //Total Transaction
              Padding(
                padding: const EdgeInsets.only(left: 730),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'TOTAL',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      Text(totalAmount().toString(),
                          style: TextStyle(
                              color: totalAmount() > customer.balance
                                  ? Colors.redAccent
                                  : Colors.white,
                              fontSize: 40,
                              fontWeight: FontWeight.bold))
                    ]),
              ),
              //Customer Credits
              Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      'AVAILABLE CREDITS',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    Text(customer.balance.toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 40,
                            fontWeight: FontWeight.bold))
                  ])
            ]));
  }

//Transaction Body
  Widget transBody() {
    return Container(
        margin: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Text(
              'CHOOSE BREW',
              style: TextStyle(fontSize: 25, color: Colors.black45),
            ),
            Container(
                width: 700,
                height: 600,
                child: ListView(children: <Widget>[
                  Wrap(
                      children: <Widget>[]..addAll(_items.map((f) {
                          return BrewItemButton(
                            model: f,
                            icon: Icons.access_alarms,
                            text: f.name,
                            onPressed: (b) {
                              print('item click');
                              //Check if Existing in TransItems
                              var r = transItems.firstWhere(
                                  (x) => x.brewID == b.name, orElse: () {
                                return null;
                              });
                              if (r != null) {
                                //Update
                                r.addQuantity();
                              } else {
                                r = TransactionModel();
                                r.setItem(b);
                                transItems.add(r);
                              }
                              setState(() {});
                            }, //editItem(b),
                          );
                        }).toList()))
                ]))
          ],
        ));
  }

  Widget transactionItems(TransactionModel t) {
    return GestureDetector(
      onTap: () {
        if (mounted) {
          setState(() {
            transItems.remove(t);
          });
        }
      },
      child: Card(
          child: Container(
        padding: EdgeInsets.all(10.0),
        child: Row(children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(t.item.coffeeBean),
              Text(t.brewID),
            ],
          ),
          Expanded(child: Container()),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                t.total.toString(),
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Text(t.quantity.toString() + ' x ' + t.price.toString())
            ],
          )
        ]),
        width: double.infinity,
        height: 60.0,
      )),
    );
  }

  //Transaction
  Widget transaction() {
    return Stack(
      children: <Widget>[
        Container(
          width: double.infinity,
          child: Column(children: <Widget>[
            Expanded(
                child: Card(
              child: ListView(
                children: <Widget>[]..addAll(transItems.map((t) {
                    return transactionItems(t);
                  }).toList()),
              ),
            )),
            Card(
              child: MaterialButton(
                color: colorGreen,
                minWidth: double.infinity,
                height: 80,
                onPressed: () {
                  transItems.forEach((f) {
                    f.customerID = customer.customerID;
                    f.custName = customer.name;
                    f.saveTransaction();
                  });
                  if (mounted) {
                    setState(() {
                      customer.updateBalance(tot, grams);
                      transItems.clear();
                    });
                  }
                  //Show Dialog
                  //InputDialog(  )
                  showDialog(
                      context: context,
                      builder: (context) {
                        return Dialog(
                          insetAnimationDuration: Duration(milliseconds: 500),
                          child: Card(
                            child: Container(
                              child: Column(
                                children: <Widget>[
                                  Icon(
                                    Icons.check_circle,
                                    size: 80,
                                    color: colorGreen,
                                  ),
                                  Center(
                                      child: Text(
                                    'Transaction Completed',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 35),
                                  )),
                                  Expanded(
                                    child: Container(),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: BasicButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      },
                                      color: colorGreen,
                                      text: 'DONE',
                                    ),
                                  )
                                ],
                              ),
                              width: 500,
                              height: 300,
                            ),
                          ),
                        );
                      });
                  //Back Screen
                  //Navigator.pop(context);
                },
                child: Text(
                  'PROCESS',
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
              ),
            )
          ]),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        header(),
        Expanded(
            child: Row(children: <Widget>[
          Stack(
            children: <Widget>[
              Opacity(
                opacity: 0.3,
                child: Container(
                  width: 740,
                  height: double.infinity,
                  child: Image(
                    image: AssetImage('assets/4.jpg'),
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              transBody(),
            ],
          ),
          Expanded(child: transaction())
        ]))
      ]),
    );
  }
}
