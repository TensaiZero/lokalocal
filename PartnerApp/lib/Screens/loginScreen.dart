import 'package:flutter/material.dart';
import 'package:loftpartnerapp/loftportalresources.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isLoading = false;

  void processLogin(UserModel u) {
    setState(() {
      _isLoading = false;
    });
    if (u != null) {
      Navigator.push(context, ScaleRoute(widget: DashboardPage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: mainColor,
            child: Column(children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: 220,
                    child: Image(
                      image: AssetImage('assets/logo.png'),
                    ),
                  )),
              Center(
                child: ZTextInputLogin(
                  numberOnly: true,
                  placeHolder: 'Partner ID',
                  value: IoC().appVM.currentUser.id.toString(),
                  onChange: (s) => IoC().appVM.currentUser.id = int.tryParse(s),
                ),
              ),
              Center(
                child: ZTextInputLogin(
                  placeHolder: 'Password',
                  value: '',
                  isPassword: true,
                ),
              ),
              MaterialButton(
                color: colorGreen,
                textColor: Colors.white,
                onPressed: () {
                  setState(() {
                    _isLoading = true;
                  });
                  IoC().appVM.currentUser.getUserData(processLogin);
                },
                child: Container(
                    width: 370,
                    height: 60,
                    child: Center(child: Text('LOGIN'))),
              )
            ]),
          ),
          LoadingOverlay(
            isLoading: _isLoading,
          )
        ],
      ),
    );
  }
}
