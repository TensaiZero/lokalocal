import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loftpartnerapp/loftportalresources.dart';
//Main Application Entry
void main(){
  //Setup IoC

  //Run Main Application
  runApp(MyApp());
}

//Main App Class
class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AppState();
}

//Main App State
class _AppState extends State<MyApp>{


  @override
  Widget build(BuildContext context) {
    //Lock Device Orientation
    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
    //Call Material App w/ MainPage
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          accentColor: Color(0xFF2DA1DB)
        ),
        home: IoC().appVM.currentPage,
        //routes: {
        //  '/login' : (context) => Scaffold(),//Login(),
        //  '/home'  : (context) => Scaffold()//Home(),
        //}
    );
  }
}
