import 'package:flutter/material.dart';

class ZTextInput extends StatelessWidget {
  final String placeHolder;
  final bool numberOnly;
  final Function(String) onChange;
  final String value;

  ZTextInput({this.placeHolder,this.numberOnly = false, this.onChange,this.value='test'});

  @override
  Widget build(BuildContext context) {
    return
      Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
        child: TextField(
          controller: TextEditingController()..text = numberOnly && value == '0.0' ? '' : value,
          onChanged: (s)=>onChange(s),
          keyboardType: numberOnly ? TextInputType.number : TextInputType.text,
          style: TextStyle( fontSize: 18.0, color: Colors.black ),
          decoration: InputDecoration(
            hintText: placeHolder,
            border: OutlineInputBorder( borderRadius: BorderRadius.all(Radius.circular(10.0) )),
            fillColor: Colors.white, 
            filled: true),
        ),
      );
  }
}

class ZTextInputLogin extends StatelessWidget {
  final String placeHolder;
  final bool numberOnly;
  final Function(String) onChange;
  final String value;
  final bool isPassword;

  ZTextInputLogin({this.placeHolder,this.numberOnly = false, this.onChange,this.value='test', this.isPassword = false});

  @override
  Widget build(BuildContext context) {
    return
      Container(
        width: 400,
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
        child: TextField(
          obscureText: isPassword,
          controller: TextEditingController()..text = numberOnly && value == '0.0' ? '' : value,
          onChanged: (s)=>onChange(s),
          keyboardType: numberOnly ? TextInputType.number : TextInputType.text,
          style: TextStyle( fontSize: 18.0, color: Colors.black ),
          decoration: InputDecoration(
            hintText: placeHolder,
            border: OutlineInputBorder( borderRadius: BorderRadius.all(Radius.circular(10.0) )),
            fillColor: Colors.white, 
            filled: true),
        ),
      );
  }
}

class InputDialog extends StatefulWidget {
  final Widget child;
  final String label;
  final BuildContext context;
  final Alignment alignment;

  InputDialog(
      {this.child,
       this.label,
       this.context,
       this.alignment = Alignment.center});

  @override
  InputDialogState createState() =>
      InputDialogState(child: child, label: label, alignment: alignment);
}

class InputDialogState extends State<InputDialog> {
  final Widget child;
  final String label;
  final Alignment alignment;
  InputDialogState({this.label, this.child, this.alignment});

  void refreshView() {
    print('RefreshDialog State');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: Align(
            alignment: alignment,
            child: Container(
              margin: EdgeInsets.all(10.0),
              child: Stack(children: <Widget>[
                labelText(),
                Padding(padding: EdgeInsets.only(top: 35.0), child: child)
              ]),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
            )));
  }

  Widget labelText() {
    return Container(
        height: 35.0,
        width: double.infinity,
        padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1.0, color: Colors.grey))),
        child: Center(child: Text(label, style: TextStyle(fontSize: 18.0))));
  }
}