import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingOverlay extends StatelessWidget {
  final bool isLoading;
  final String text;

  LoadingOverlay({this.isLoading, this.text = ''});

  Widget loadingOnly() {
    return Container(
        color: Colors.black45,
        child: Center(child: CupertinoActivityIndicator()));
  }

  Widget loadingwithText() {
    return Container(
      color: Colors.black45,
      child: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CupertinoActivityIndicator(),
              Text(text, style: TextStyle(color: Colors.white, fontSize: 16.0))
            ]),
      ),
    );
  }

  Widget loadingWidget() {
    return text == '' ? loadingOnly() : loadingwithText();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading ? loadingWidget() : Container();
  }
}