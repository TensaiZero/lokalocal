import 'package:flutter/material.dart';
import 'package:loftpartnerapp/loftportalresources.dart';

class DashboardButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final IconData icon;

  DashboardButton({this.onPressed, this.text, this.icon});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: MaterialButton(
          elevation: 5.0,
          onPressed: onPressed,
          color: Color(0xFF2DA1DB).withOpacity(0.5),
          height: 250.0,
          minWidth: 250.0,
          child: Container(
            height: 200.0,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Icon(
                      icon,
                      size: 50.0,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    text,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0),
                  ),
                ]),
          )),
    );
  }
}

class BrewItemButton extends StatelessWidget {
  final Function(BrewItemModel) onPressed;
  final String text;
  final IconData icon;
  final BrewItemModel model;

  BrewItemButton({this.onPressed, this.text, this.icon, this.model});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: MaterialButton(
          elevation: 5.0,
          onPressed: () => onPressed(model),
          color: Color(0xFF2DA1DB).withOpacity(0.5),
          height: 200.0,
          minWidth: 200.0,
          child: Container(
            height: 200.0,
            child: Stack(children: <Widget>[
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      //child: Icon ( icon, size: 50.0, color: Colors.white,  ),
                    ),
                    Text(
                      model.name,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0),
                    ),
                    Text(
                      model.coffeeBean + ' (' + model.size + ')',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ]),
              Container(
                padding: EdgeInsets.only( top: 150, left: 110 ),
                child: Text(model.price.toString(), style: TextStyle( color: Colors.white, fontSize: 25 ),),
              )
            ]),
          )),
    );
  }
}

class BasicButton extends StatelessWidget {
  final Function onPressed;
  final Color color;
  final String text;

  BasicButton({this.color, this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      child: Text(text, style: TextStyle(fontSize: 16.0, color: Colors.white)),
      color: color,
      minWidth: 200,
      height: 50,
      elevation: 10.0,
    );
  }
}
