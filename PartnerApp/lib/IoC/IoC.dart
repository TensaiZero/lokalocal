import 'package:loftpartnerapp/loftportalresources.dart';

//Inversion of Control (Singleton)
class IoC {
  static final IoC _singleton = IoC._internal();
  //static UI _ui = UI();
  static AppVM  _appVM = AppVM();

  static void configure(){

  }

  factory IoC() {
    return _singleton;
  }

  AppVM get appVM => _appVM;
  //UI get ui => _ui;

  IoC._internal();
}