import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loft_client_portal/model/user.dart';
import 'package:loft_client_portal/util/qr_code/qr_image.dart';
import 'package:loft_client_portal/util/common_widgets.dart' as common;
import 'package:loft_client_portal/util/global_att.dart' as global;
import 'dart:math' as math;

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool _prompt = false;

  @override
  void initState() {
    if (global.customer.customer_id == 0) {
      _prompt = false;
      getCustomer();
    }
    global.credits = 0;
  }

  void refresh(User user) {
    global.customer = user;
//    global.contributions = global.customer.grams_consumed ~/ global.gramPer;
    global.contributions = int.parse((global.customer.grams_consumed * 0.05).toStringAsFixed(0));

    if (mounted) {
      setState(() {});

      if (_prompt) {
        showDialog(
          context: context,
          barrierDismissible: false,
          child: new Dialog(
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 350.0,
                  child: Column(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.all(10.0)),
                      Image.asset("images/loft_logo_grey.png", height: 40.0),
                      Padding(padding: EdgeInsets.all(10.0)),
                      Text("Transaction Complete",
                          style: common.loftStyleBlue2),
                      Padding(padding: EdgeInsets.all(20.0)),
                      Image.asset("images/coffee_bean.png", height: 50.0),
                      Padding(padding: EdgeInsets.all(20.0)),
                      Text("Thank you for your purchase",
                          style: common.loftStyleGrey1),
                      Padding(padding: EdgeInsets.all(20.0)),
                      FlatButton(
                        disabledColor: common.loftBlue1,
                        child: Text("OK"),
                        onPressed: () => Navigator.pop(context),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      }
    }
  }

  void getCustomer() {
    Firestore.instance
        .collection('customers')
        .where('customerID', isEqualTo: global.customerID)
        .snapshots()
        .listen((data) {
      data.documents.forEach((f) {
        refresh(new User(
            f['customerID'],
            f['name'],
            f['loft_balance'] == null ? "0.0" : double.parse(f['loft_balance']),
            f['grams_consumed'] == null
                ? "0.0"
                : double.parse(f['grams_consumed'])));
        _prompt = true;
      });
    });
  }

  Widget _mainBody() {
    return Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              height: 200.0,
              color: Colors.white,
              child: Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 1.0,
                    child: Image.asset("images/loft_picture2.jpeg"),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 80.0),
                    child: Center(
                      child: Container(
                          child: CircleAvatar(
//                            radius: 100.0,
                            backgroundColor: Colors.white,
                            child: Icon(
                              Icons.person,
                              size: 80.0,
                              color: common.loftBlue1,
                            ),
                          ),
                          width: 100.0,
                          height: 100.0,
                          padding: const EdgeInsets.all(2.0),
                          decoration: new BoxDecoration(
                            color: common.loftBlue1, // border color
                            shape: BoxShape.circle,
                          )),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  Text("${global.customer.customer_name}",
                      style: common.loftStyleGrey2),
                  Padding(padding: EdgeInsets.all(2.0)),
                  Text("${global.customer.customer_id}",
                      style: common.loftStyleGrey1),
                  Padding(padding: EdgeInsets.all(15.0)),
                  QrImage(
                    data: global.customerID.toString(),
                    size: 150.0,
                  ),
                  Padding(padding: EdgeInsets.all(50.0)),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("You have ", style: common.loftStyleGrey2),
                      Text("${global.customer.loft_credits.toString()} ",
                          style: common.loftStyleGrey2),
                      Image.asset(
                        "images/loft_logo_3.PNG",
                        height: 25.0,
                      ),
                      Text(" credits", style: common.loftStyleGrey2),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _mainBody(),
    );
  }
}
