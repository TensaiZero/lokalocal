import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loft_client_portal/ui/MainScreen.dart';
import 'package:loft_client_portal/util/common_widgets.dart' as common;
import 'package:loft_client_portal/util/global_att.dart' as global;

class LoginScreen extends StatelessWidget {
  final customerID = new TextEditingController();
  final passKey = new TextEditingController();

  void onSubmit(BuildContext context) {
    global.transList.clear();
    global.customerID = int.parse(customerID.text);

    showDialog(
      context: context,
      barrierDismissible: false,
      child: new Dialog(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 40.0,
              child: new CircularProgressIndicator(),
            ),
            new Text("Loading"),
          ],
        ),
      ),
    );

    new Future.delayed(new Duration(seconds: 2), () {
      Navigator.pop(context); //pop dialog
      Navigator.push(
        context,
        PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) {
              return MainScreen();
            },
            transitionsBuilder: (context, animation1, animation2, child) {
              return FadeTransition(
                  opacity:
                      Tween<double>(begin: 0.0, end: 1.0).animate(animation1),
                  child: child);
            },
            transitionDuration: Duration(milliseconds: 800)),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: common.loftBlue1,
      body: ListView(
        children: <Widget>[
          Container(
              child: Image.asset(
            "images/loft_logo.png",
          )),
          Container(
              padding: EdgeInsets.all(50.0),
              child: Column(
                children: <Widget>[
                  new Theme(
                    data: new ThemeData(
                        primaryColor: Colors.white,
                        accentColor: Colors.white,
                        hintColor: Colors.white),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "Customer ID",
                      ),
                      controller: customerID,
                      cursorColor: Colors.white,
                    ),
                  ),
                  new Theme(
                    data: new ThemeData(
                        primaryColor: Colors.white,
                        accentColor: Colors.white,
                        hintColor: Colors.white),
                    child: TextField(
                      decoration: InputDecoration(hintText: "Passkey"),
                      obscureText: true,
                      controller: passKey,
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(20.0)),
                  FlatButton(
                    color: common.loftBlue1,
                    child: Text("SUBMIT", style: common.loftStyleWhite1),
                    onPressed: () => onSubmit(context),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
