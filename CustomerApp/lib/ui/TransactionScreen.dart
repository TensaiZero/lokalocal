import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loft_client_portal/model/brewitem.dart';
import 'package:loft_client_portal/model/transactionitem.dart';
import 'package:loft_client_portal/util/global_att.dart' as global;
import 'package:loft_client_portal/util/common_widgets.dart' as common;
import 'package:intl/intl.dart';

class TransactionScreen extends StatefulWidget {
  @override
  _TransactionScreenState createState() => _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {
  @override
  void initState() {
    if (global.brewList.isEmpty) {
      getBrews();
    }
    if (global.transList.isEmpty) {
      getTransactions();
    }
  }

  void refreshTransactions(TransactionItem transItem) {
//    if(global.activePage != 2)
//      return;
    global.transList.add(transItem);
    if (mounted) setState(() {});
  }

  void refreshBrews(BrewItem brewItem) {
    global.brewList.add(brewItem);
  }

  String transDate(String dateTime) {
    return DateFormat('yyyy-MM-dd – kk:mm')
        .format(DateTime.parse(dateTime))
        .toString();
  }

  void getTransactions() {
    global.transList.clear();
    Firestore.instance
        .collection('transactions')
        .where('customerID', isEqualTo: global.customerID)
        .snapshots()
        .listen((data) => data.documents.forEach((f) => refreshTransactions(
            new TransactionItem(
                f['customerID'],
                f['date'],
                f['time'],
                f['branch'],
                f['partnerID'],
                f['partnerName'],
                f['brewID'],
                f['bean'],
                f['quantity'],
                f['unit'],
                f['size'],
                f['cost'],
                f['order_qty']))));
    print(global.customerID.toString());
  }

  void getBrews() {
    global.transList.clear();
    Firestore.instance.collection('brews').snapshots().listen((data) =>
        data.documents.forEach((f) => refreshBrews(new BrewItem(
            f['bean'],
            f['brewID'],
            f['cost'],
            f['partnerID'],
            f['quantity'],
            f['size'],
            f['unit']))));
  }

  Widget _mainBody() {
    return //global.transList.isNotEmpty ?
        Column(
      children: <Widget>[
        Flexible(
          child: new ListView.builder(
            itemBuilder: (BuildContext context, int index) => new Container(
                color: Colors.white,
                child: Container(
                  padding: EdgeInsets.all(15.0),
                  child: GestureDetector(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Colors.grey,
                            radius: 26.0,
                            child: Text(
                                "${global.transList[index].quantity}"
                                "${global.transList[index].unit}",
                                style: common.loftStyleWhite1),
                          ),
                          Padding(padding: EdgeInsets.all(5.0)),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "${global.transList[index].brew_id}"
                                      " (${global.transList[index].bean})",
                                  style: common.loftStyleGrey4,
                                ),
                                Text(
                                  "${global.transList[index].partner_name} - "
                                      " ${global.transList[index].branch}",
                                  style: common.loftStyleGrey3,
                                ),
                                Text(
                                  "${global.transList[index].date}"
                                      " ${global.transList[index].time}",
                                  style: common.loftStyleGrey3,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            alignment: Alignment.topRight,
                            child: Text(
                              "${global.transList[index].credit_cost}",
                              style: common.loftStyleBlack3,
                            ),
                          )
                        ]),
                  ),
                )),
            itemCount: global.transList.length,
          ),
        ),
      ],
    );
  }

  Future<void> _refreshTransactions() async {
    getTransactions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: common.loftBlue1,
        title: Text("Order History"),
      ),
      backgroundColor: Colors.white,
      body:
          RefreshIndicator(child: _mainBody(), onRefresh: _refreshTransactions),
    );
  }
}
