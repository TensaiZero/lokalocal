import 'package:flutter/material.dart';
import 'package:loft_client_portal/model/user.dart';
import 'package:loft_client_portal/ui/ProfileScreen.dart';
import 'package:loft_client_portal/ui/ProgramScreen.dart';
import 'package:loft_client_portal/ui/TransactionScreen.dart';
import 'package:loft_client_portal/util/database_helper.dart';
import 'package:loft_client_portal/util/global_att.dart';
import 'package:loft_client_portal/util/common_widgets.dart' as common;
import 'package:loft_client_portal/util/global_att.dart' as global;

class MainScreen extends StatefulWidget {
  MainScreen({Key key, @required this.pageSelected}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();

  Page pageSelected;
}

class _MainScreenState extends State<MainScreen> {
  var db = new DatabaseHelper();

  @override
  void initState() {
    global.gramPer = 2;
    global.customer = new User(0, "", 0, 0.0);

    if (widget.pageSelected == null) {
      widget.pageSelected = Page.profile;
    }
  }

  void refresh() {
    setState(() {});
  }

  void _onPageSelected(Page selection) {
    setState(() {
      widget.pageSelected = selection;
    });
  }

  void _onSelectPage(int index) {
    global.activePage = index;
    switch (index) {
      case 0:
        _onPageSelected(Page.profile);
//        Navigator.push(context,SlideRightRoute(widget: MainScreen(pageSelected: Page.profile)));
        break;
      case 1:
        _onPageSelected(Page.program);
//        Navigator.push(context,SlideLeftRoute(widget: MainScreen(pageSelected: Page.program)));
        break;
      case 2:
        _onPageSelected(Page.transactions);
//        Navigator.push(context,SlideLeftRoute(widget: MainScreen(pageSelected: Page.transactions)));
        break;
    }
  }

  Widget _mainBody() {
    switch (widget.pageSelected) {
      case Page.profile:
        return ProfileScreen();
      case Page.program:
        return ProgramScreen();
      case Page.transactions:
        return TransactionScreen();
    }
    return null;
  }

  Color _colorTabMatching({Page pageSelected}) {
    return widget.pageSelected == pageSelected ? common.loftBlue1 : Colors.grey;
  }

  BottomNavigationBarItem _barItem(
      {IconData icon, Page pageSelected, String text}) {
    return BottomNavigationBarItem(
      icon: Icon(
        icon,
        color: _colorTabMatching(pageSelected: pageSelected),
        size: 25.0,
      ),
      title: Text("$text",
          style: TextStyle(
              color: _colorTabMatching(pageSelected: pageSelected),
              fontSize: 14.0)),
    );
  }

  Widget _mainBottomNavigationBar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: [
        _barItem(
            icon: Icons.account_circle,
            pageSelected: Page.profile,
            text: "Profile"),
        _barItem(icon: Icons.toys, pageSelected: Page.program, text: "Program"),
        _barItem(
            icon: Icons.format_list_bulleted,
            pageSelected: Page.transactions,
            text: "Order History")
      ],
      onTap: _onSelectPage,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _mainBody(),
      bottomNavigationBar: _mainBottomNavigationBar(),
    );
  }
}
