import 'package:flutter/material.dart';
import 'package:loft_client_portal/util/common_widgets.dart' as common;
import 'package:loft_client_portal/res/strings.dart' as strings;
import 'package:loft_client_portal/util/global_att.dart' as global;

class ProgramScreen extends StatefulWidget {
  @override
  _ProgramScreenState createState() => _ProgramScreenState();
}

class _ProgramScreenState extends State<ProgramScreen> {



  Widget _mainBody() {
    return Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              height: 150.0,
              color: Colors.white,
              child: Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 1.0,
                    child: Image.asset("images/coffee_2.jpeg"),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
//                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset("images/loft_logo_grey.png", height: 35.0),
//                  Text("Gives Back to Coffee Farmers"),
                  Text("GIVES BACK TO COFFEE FARMERS", style: common.loftStyleGrey3),
                  Container(
                    padding: EdgeInsets.all(40.0),
                    child: Text(
                      strings.info,
                      style: common.loftStyleGrey1,
                      textAlign: TextAlign.center,
                    ),
                  ),
//                  Padding(padding: EdgeInsets.all(10.0)),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Image.asset("images/coffee_bean.png",
//                          height: 25.0),
//                      Text(" = ${global.gramPer}g Consumed", style: common.loftStyleGrey3),
//                    ],
//                  ),
                  Padding(padding: EdgeInsets.all(15.0)),
                  Text("Your Contributions", style: common.loftStyleGrey1),
                  Padding(padding: EdgeInsets.all(10.0)),
                  Center(
//                      color: Colors.white,
//                      padding: EdgeInsets.all(5.0),
//                      height: 45.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset("images/coffee_bean.png", height: 50.0),
                          Text(" x ${global.contributions}", style: common.loftStyleGrey5,)
                        ],
                      )
                  ),
                  Padding(padding: EdgeInsets.all(8.0)),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _mainBody(),
    );
  }


}
