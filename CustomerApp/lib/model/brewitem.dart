class BrewItem {
  int _id, _partner_id;
  String _bean, _brew_id, _cost, _quantity, _size, _unit;

  BrewItem(this._bean, this._brew_id, this._cost, this._partner_id,
      this._quantity, this._size, this._unit);

  BrewItem.map(dynamic obj) {
    this._bean = obj['bean'];
    this._brew_id = obj['brew_id'];
    this._cost = obj['cost'];
    this._partner_id = obj['partner_id'];
    this._quantity = obj['quantity'];
    this._size = obj['size'];
    this._unit = obj['unit'];
    this._id = obj['id'];
  }

  // getters to make it public
  String get bean => _bean;
  String get brew_id => _brew_id;
  String get cost => _cost;
  int get partner_id => _partner_id;
  String get quantity => _quantity;
  String get size => _size;
  String get unit => _unit;
  int get id => _id;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["bean"] = _bean;
    map["brew_id"] = _brew_id;
    map["cost"] = _cost;
    map['partner_id'] = _partner_id;
    map["quantity"] = _quantity;
    map["size"] = _size;
    map["unit"] = _unit;
    if (id != null) {
      map["id"] = _id;
    }
    return map;
  }

  BrewItem.fromMap(Map<String, dynamic> map) {
    this._bean = map["bean"];
    this._brew_id = map["brew_id"];
    this._cost = map["cost"];
    this._partner_id = map["partner_id"];
    this._quantity = map["quantity"];
    this._size = map["size"];
    this._unit = map["unit"];
    this._id = map["id"];
  }
}
