import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  int _id,  _customer_id;
  String _customer_name;
  double _grams_consumed, _loft_credits;

  User(this._customer_id, this._customer_name, this._loft_credits,
      this._grams_consumed);

  User.map(dynamic obj) {
    this._customer_id = obj['customer_id'];
    this._customer_name = obj['customer_name'];
    this._loft_credits = obj['loft_credits'];
    this._grams_consumed = obj['grams_consumed'];
    this._id = obj['id'];
  }

  // getters to make it public
  int get customer_id => _customer_id;
  String get customer_name => _customer_name;
  double get loft_credits => _loft_credits;
  double get grams_consumed => _grams_consumed;
  int get id => _id;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["customer_id"] = _customer_id;
    map["customer_name"] = _customer_name;
    map["loft_credits"] = _loft_credits;
    map["grams_consumed"] = _grams_consumed;
    if (id != null) {
      map["id"] = _id;
    }
    return map;
  }

  User.fromMap(Map<String, dynamic> map) {
    this._customer_id = map["customer_id"];
    this._customer_name = map["customer_name"];
    this._loft_credits = map["loft_credits"];
    this._grams_consumed = map["grams_consumed"];
    this._id = map["id"];
  }

//  void parseData(dynamic o) {
//    _customer_name = o['name'];
//    print(_customer_name);
//  }
//
//  void getUserData(){
//    Firestore.instance.collection('customers')
//        .where('customerID', isEqualTo: 100045)
//        .snapshots().listen((data)=> data.documents.forEach((f) => parseData(f)));
//  }
}
