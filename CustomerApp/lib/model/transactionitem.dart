import 'package:loft_client_portal/model/brewitem.dart';

class TransactionItem {
  int _id, _customer_id, _partner_id;
  String _date, _time, _branch, _partner_name, _brew_id, _bean,
        _quantity, _unit, _size, _credit_cost, _order_qty;

  TransactionItem(this._customer_id, this._date, this._time, this._branch,
    this._partner_id, this._partner_name, this._brew_id, this._bean,
    this._quantity, this._unit, this._size, this._credit_cost, this. _order_qty);

  TransactionItem.map(dynamic obj) {
    this._customer_id = obj['customer_id'];
    this._date = obj['date'];
    this._time = obj['time'];
    this._branch = obj['branch'];
    this._partner_id = obj['partner_id'];
    this._partner_name = obj['partner_name'];
    this._brew_id = obj['brew_id'];
    this._bean = obj['bean'];
    this._quantity = obj['quantity'];
    this._unit = obj['unit'];
    this._size = obj['size'];
    this._credit_cost = obj['credit_cost'];
    this._order_qty = obj['order_qty'];
    this._id = obj['id'];
  }

  // getters to make it public
  int get customer_id => _customer_id;
  String get date => _date;
  String get time => _time;
  String get branch => _branch;
  int get partner_id => _partner_id;
  String get partner_name => _partner_name;
  String get brew_id => _brew_id;
  String get bean => _bean;
  String get quantity => _quantity;
  String get unit => _unit;
  String get size => _size;
  String get credit_cost => _credit_cost;
  String get order_qty => _order_qty;
  int get id => _id;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["customer_id"] = _customer_id;
    map["date"] = _date;
    map["time"] = _time;
    map["branch"] = _branch;
    map["partner_id"] = _partner_id;
    map['partner_name'] = _partner_name;
    map["brew_id"] = _brew_id;
    map["bean"] = _bean;
    map["quantity"] = _quantity;
    map["unit"] = _unit;
    map["size"] = _size;
    map["credit_cost"] = _credit_cost;
    map["order_qty"] = _order_qty;
    if (id != null) {
      map["id"] = _id;
    }
    return map;
  }

  TransactionItem.fromMap(Map<String, dynamic> map) {
    this._customer_id = map['customer_id'];
    this._date = map['date'];
    this._time = map['time'];
    this._branch = map['branch'];
    this._partner_id = map['partner_id'];
    this._partner_name = map['partner_name'];
    this._brew_id = map['brew_id'];
    this._bean = map['bean'];
    this._quantity = map['quantity'];
    this._unit = map['unit'];
    this._size = map['size'];
    this._credit_cost = map['credit_cost'];
    this._order_qty = map['order_qty'];
    this._id = map['id'];
  }
}
