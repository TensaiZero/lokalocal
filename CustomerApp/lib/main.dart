import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loft_client_portal/ui/LoginScreen.dart';
import 'package:loft_client_portal/ui/MainScreen.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "",
//    home: MainScreen(),
    home: LoginScreen(),
  ));
}