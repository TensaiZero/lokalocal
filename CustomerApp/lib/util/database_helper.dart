import 'dart:io';
import 'package:loft_client_portal/model/transactionitem.dart';
import 'package:loft_client_portal/model/user.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class DatabaseHelper {
  //we do not want to create an object each time
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance; //consolidating

  final String customerTable = "dcustomer";
  final String partnerTable = "dpartner";
  final String transactionTable = "dtransactions";
  final String brewTable = "dbrew";

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb(); //initialize database
    return _db;
  }

  DatabaseHelper.internal(); // private to the class - can be any name

  initDb() async {

    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path,
        "localdb.db");

    var ourDb = await openDatabase(path,
        version: 1, onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute("""
            CREATE TABLE $customerTable (
              id INTEGER PRIMARY KEY, 
              customer_id TEXT NOT NULL UNIQUE,
              passkey TEXT,
              customer_name TEXT,
              loft_credits TEXT,
              grams_consumed REAL
            )""");

    await db.execute("""
            CREATE TABLE $transactionTable (
              id INTEGER PRIMARY KEY,
              branch TEXT NOT NULL UNIQUE,
              customer_id TEXT,
              datetime TEXT,
              partner_id TEXT,
              brew_id TEXT,
              credit_cost REAL
            )""");
  }

  //Insert
  Future<int> upsertUser(User user) async {
    var dbClient = await db;
    int res;
    //    int res = await dbClient.insert("$userTable", user.toMap());
    //    return res;
    var count = Sqflite.firstIntValue(await dbClient.rawQuery(
            "SELECT COUNT(*) FROM $customerTable WHERE customer_id = ?", [user.customer_id]));
    if (count == 0) {
      res = await dbClient.insert("$customerTable", user.toMap());
    } else {
      res = await dbClient.update("$customerTable", user.toMap(),
          where: "id = ?", whereArgs: [user.id]);
    }
    return res;
  }

  Future<int> upsertTransactions(TransactionItem transactionItem) async {
    var dbClient = await db;
    int res;
    var count = Sqflite.firstIntValue(await dbClient.rawQuery(
        "SELECT COUNT(*) FROM $transactionTable WHERE id = ?", [transactionItem.id]));
    if (count == 0) {
      res = await dbClient.insert("$transactionTable", transactionItem.toMap());
    } else {
      res = await dbClient.update("$transactionTable", transactionItem.toMap(),
          where: "id = ?", whereArgs: [transactionItem.id]);
    }
    return res;
  }

////  //Get users
////  Future<List> getAllUsers() async {
////    var dbClient = await db;
////    var result = await dbClient.rawQuery("SELECT * FROM $userTable");
////    return result.toList();
////  }
//
//  Future<int> getUserCount() async {
//    var dbClient = await db;
//    return Sqflite.firstIntValue(
//        await dbClient.rawQuery(
//            "SELECT COUNT (*) FROM $userTable"
//        )
//    );
//  }

//  Future<int> getMenuCount() async {
//    var dbClient = await db;
//    return Sqflite.firstIntValue(
//        await dbClient.rawQuery(
//            "SELECT COUNT (*) FROM $menuTable"
//        )
//    );
//  }
//
//  Future<List> getMenuList({String category}) async {
//    var dbClient = await db;
//    var result;
//    if(category == "ALL") {
//      result = await dbClient.rawQuery(
//          "SELECT * FROM $menuTable");
//    }
//    else {  result = await dbClient.rawQuery(
//          "SELECT * FROM $menuTable WHERE category = '$category'");
//    }
//    return result.toList();
//  }
//
//  Future<List> getCategoryList() async {
//    var dbClient = await db;
//    var result = await dbClient.rawQuery("SELECT * FROM $categoryTable");
//    return result.toList();
//  }

//  // get last order number
//  Future<OrderHdr> getLastOrder() async {
//    var dbClient = await db;
//    var result = await dbClient.rawQuery(
//        "SELECT * FROM $orderhTable ORDER BY order_no DESC LIMIT 1");
//    if(result.length == 0) return null;
//    return new OrderHdr.fromMap(result.first);
//  }

//  // delete user
//  Future<int> deleteUser(int id) async {
//    var dbClient = await db;
//    return await dbClient.delete(userTable,
//        where: "$columnId = ?", whereArgs: [id]);
//  }
//
//  // update user
//  Future<int> updateUser(User user) async {
//    var dbClient = await db;
//    return await dbClient.update(userTable,
//        user.toMap(), where: "$columnId = ?", whereArgs: [user.id]);
//  }
//
//  Future close() async {
//    var dbClient = await db;
//    return dbClient.close();
//  }
}
