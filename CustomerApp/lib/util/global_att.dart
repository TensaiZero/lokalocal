import 'package:loft_client_portal/model/brewitem.dart';
import 'package:loft_client_portal/model/transactionitem.dart';
import 'package:loft_client_portal/model/user.dart';

enum Page {
  profile,
  program,
  transactions
}
int activePage;
int contributions;

int credits = 400;

int customerID;

User customer = new User(0, "", 0, 0.0);

List<TransactionItem> transList = <TransactionItem>[];
List<BrewItem> brewList = <BrewItem>[];

int gramPer;