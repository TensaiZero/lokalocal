import 'package:flutter/material.dart';

final loftBlue1 = Color(0xFF2DA1DB);

// Styles
final loftStyleBlack1 = const TextStyle(
    fontSize: 20.0, color: Colors.black);

final loftStyleBlack2 = const TextStyle(
    fontSize: 16.0, color: Colors.black);

final loftStyleBlack3 = const TextStyle(
    fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.w500);

final loftStyleGrey1 = const TextStyle(
    fontSize: 16.0, color: Colors.grey);

final loftStyleGrey2 = const TextStyle(
    fontSize: 18.0, color: Colors.grey);

final loftStyleGrey3 = const TextStyle(
    fontSize: 14.0, color: Colors.grey);

final loftStyleGrey4 = const TextStyle(
    fontSize: 14.0, fontWeight: FontWeight.w600, color: Colors.black54);

final loftStyleGrey5 = const TextStyle(
    fontSize: 24.0, color: Colors.grey);

final loftStyleGrey6 = const TextStyle(
    fontSize: 34.0, color: Colors.grey);

final loftStyleBlue1 = const TextStyle(
    fontSize: 16.0, color: Color(0xFF2DA1DB));

final loftStyleBlue2 = const TextStyle(
    fontSize: 20.0, color: Color(0xFF2DA1DB));

final loftStyleWhite1 = const TextStyle(
    fontSize: 20.0, color: Colors.white);

