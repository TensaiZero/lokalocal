final String info =
    "As one of the fastest growing co-working spaces in the metro, "
"LOFT has always been proud of the coffee we serve our guests, and in the "
"interest of giving back to the community of coffee farmers in the country, "
"for every gram of coffee we serve you, a percentage of each sale will go back to our "
"local coffee farmers.";